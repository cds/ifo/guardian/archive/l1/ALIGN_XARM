# Docstring:
"""
Specialisation of the ALIGN_ARM guardian script for the X arm.

Author: Nathan Holland, A. Mullavey.
Date: 2019-05-07
Contact: nathan.holland@ligo.org

Modified: 2019-05-07 (Created)
Modified: 2019-05-08 (Continued writing first iteration).
Modified: 2019-05-09 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-13 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-14 (Continued writing first iteration; 
                      Planning in code comments;
                      Minor debugging).
Modified: 2019-05-21 (Debugging and testing).
Modified: 2019-05-28 (Debugging and testing).
Modified: 2019-05-31 (Changed the als arm manager to now be defined in the 
                      scope of this script. I don't know why I didn't 
                      previously think of this solution, or if it will work.).
Modified: 2019-06-03 (Adapt to PEP8/Guardian Style).
Modified: 2019-06-04 (Temporarily remove als_loose from automatic control.).
Modified: 2019-06-11 (Debugging, and added als_loose_control back in.).
Modified: 2019-06-17 (Adjust power levels to using argparse.Namespace objects).
Modified: 2019-06-18 (Debugging and testing.).
Modified: 2019-06-28 (Added stop conditions for TMS and ITM alignment.)
"""


#------------------------------------------------------------------------------
# Imports:

# Import the simple namespace class form argparse.
from argparse import Namespace

# Import the class Optic from 
# /opt/rtcds/userapps/release/asc/l1/guardian/optic.py
from optic import Optic

# The node class to manage the ALS arm guardian.
from guardian import Node

from os import path
import csv

# Import the base X/Y arm code.
import ALIGN_ARM

#------------------------------------------------------------------------------

align_dir = '/opt/rtcds/userapps/release/asc/l1/guardian/'

arm = "X"
tms = "TMS"+arm
itm = "ITM"+arm
etm = "ETM"+arm
pr2 = "PR2"
 
# The arm being aligned.
ALIGN_ARM.arm = arm #'X'

# The test mass being aligned.
ALIGN_ARM.TMS = tms   #"TMSX"
ALIGN_ARM.ITM = itm   #"ITMX"
ALIGN_ARM.ETM = etm   #"ETMX"

# Convenient access for the optics.
ALIGN_ARM.tms = Optic(tms)  #Optic("TMSX")
ALIGN_ARM.itm = Optic(itm)  #Optic("ITMX")
ALIGN_ARM.etm = Optic(etm)  #Optic("ETMX")
ALIGN_ARM.pr2 = Optic(pr2)

# The ALS arm guardian to manage.
ALIGN_ARM.als_loose = Node("ALS_XARM")

#------------------------------------------------------------------------------
### TMS ###

# The oscillator number for the TMS alignment.
ALIGN_ARM.tms_osc_number = "9"
ALIGN_ARM.tms_osc_row = "11"
ALIGN_ARM.tms_demod_sensor = "8"
ALIGN_ARM.tms_servo_row = "11"

ALIGN_ARM.tms_demod_phase = {"PIT":117,
                             "YAW":69
                            }

ALIGN_ARM.tms_dof_gain = {"PIT":2.0,
                          "YAW":2.0
                         }

# The optimised alignment values for the TMS bench.
# Approximately points the TMS bench to the ITM baffle PD.
ALIGN_ARM.tms_pit = -5300
ALIGN_ARM.tms_yaw = 1695

filename = "tms{0}_bpd1_offsets.csv".format(arm.lower())
if path.exists(align_dir + filename):
    with open(align_dir + filename,"r") as csvfile:
        reader = csv.DictReader(csvfile)
        list = []
        for row in reader:
            list.append(dict(row))
        bpd1_ofs = list[-1]
    bpd1_ofs["P"] = float(bpd1_ofs["P"])
    bpd1_ofs["Y"] = float(bpd1_ofs["Y"])
    ALIGN_ARM.tms_bpd1_ofs = bpd1_ofs  
else:
    ALIGN_ARM.tms_bpd1_ofs = {'P':-25.72,'Y':8.02} #{'P':-26.03,'Y':8.20}

#ALIGN_ARM.tms_bpd1_ofs = {'P':-25.72,'Y':8.02} #{'P':-26.03,'Y':8.20}

# Stop, and implicit start, conditions for automatic TMS alignment.
_tms_stop = {"pwr_mn" : 0.5e-3,     # Minimum baffle PD power.
             "pwr_ok" : 4.0e-3, # Acceptable baffle PD power.	# Was 1.8e-3 ajm190813
             "grd_ok" : 1.0e-5,  # Maximum level for power gradient.
             "pit_ok" : 1.0e-4,  # Maximum level for Pit error signal.
             "yaw_ok" : 1.0e-4   # Maximum level for Yaw error signal.
            }
ALIGN_ARM.tms_stop = Namespace(**_tms_stop)

# The gains for the TMS alignment dithering.
ALIGN_ARM.tms_G_pit = -0.8
ALIGN_ARM.tms_G_yaw = -0.3

#------------------------------------------------------------------------------
### ITM ###

# The optimised alignment values for the ITM.
# Approximately points the ITM to the ETM baffle PD.
# Must be adjusted by the gain of the filter module.
ALIGN_ARM.itm_pit_ofs = -360.0
ALIGN_ARM.itm_yaw_ofs = -850.0

filename = "itm{0}_bpd1_offsets.csv".format(arm.lower())
if path.exists(align_dir + filename):
    with open(align_dir + filename,"r") as csvfile:
        reader = csv.DictReader(csvfile)
        list = []
        for row in reader:
            list.append(dict(row))
        bpd1_ofs = list[-1]
    bpd1_ofs["P"] = float(bpd1_ofs["P"])
    bpd1_ofs["Y"] = float(bpd1_ofs["Y"])
    ALIGN_ARM.itm_bpd1_ofs = bpd1_ofs  
else:
    ALIGN_ARM.itm_bpd1_ofs = {'P':-17.89,'Y':-18.40} #{'P':-16.58,'Y':-17.74}

#ALIGN_ARM.itm_bpd1_ofs = {'P':-17.89,'Y':-18.40} #{'P':-16.58,'Y':-17.74}

# Stop, and implicit start, conditions for automatic TMS alignment.
_itm_stop = {"pwr_mn" : 0.2e-3,     # Minimum baffle PD power.
             "pwr_ok" : 1.4e-3,  # Acceptable baffle PD power.    # Was 7.15e-4 ajm190813
             "grd_ok" : 8.0e-6,  # Maximum level for power gradient.
             "pit_ok" : 8.0e-5,  # Maximum level for Pit error signal.
             "yaw_ok" : 8.0e-5   # Maximum level for Yaw error signal.
            }
ALIGN_ARM.itm_stop = Namespace(**_itm_stop)

# The gains for the ITM alignment dithering.
ALIGN_ARM.itm_G_pit = 0.5
ALIGN_ARM.itm_G_yaw = -0.5

#------------------------------------------------------------------------
### ETM ###
#TODO find correct values

# The optimised alignment values for the ITM.
# Approximately points the ITM to the ETM baffle PD.
# Must be adjusted by the gain of the filter module.

filename = "etm{0}_bpd1_offsets.csv".format(arm.lower())
if path.exists(align_dir + filename):
    with open(align_dir + filename,"r") as csvfile:
        reader = csv.DictReader(csvfile)
        list = []
        for row in reader:
            list.append(dict(row))
        bpd1_ofs = list[-1]
    bpd1_ofs["P"] = float(bpd1_ofs["P"])
    bpd1_ofs["Y"] = float(bpd1_ofs["Y"])
    ALIGN_ARM.etm_bpd1_ofs = bpd1_ofs  
else:
    ALIGN_ARM.etm_bpd1_ofs = {'P':-18.41,'Y':17.68} #{'P':-18.93,'Y':17.92}

#ALIGN_ARM.etm_bpd1_ofs = {'P':-18.41,'Y':17.68} #{'P':-18.93,'Y':17.92}

# Acceptable power level for final alignment.
_etm_stop = {"pwr_mn" : 0.2e-3,    # Minimum baffle PD power.
             "pwr_ok" : 1.4e-3, # Acceptable baffle PD power.    # Was 7.15e-4 ajm190813
             "grd_ok" : 8.0e-6, # Maximum level for power gradient.
             "pit_ok" : 8.0e-5, # Maximum level for Pit error signal.
             "yaw_ok" : 8.0e-5  # Maximum level for Yaw error signal.
            }
ALIGN_ARM.etm_stop = Namespace(**_etm_stop)

# The gains for the ETM alignment dithering. TODO: find correct gains
ALIGN_ARM.etm_G_pit = -0.5	#-0.5
ALIGN_ARM.etm_G_yaw = -0.5	#-0.5

#------------------------------------------------------------------------
### Arm Cavity Alignment ###

#TODO: find these values
_arm_stop = {"pwr_min" : 600.0,
             "pwr_aligned" : 1200.0,
             "pit_ok" : 200.0,
             "yaw_ok" : 150.0,
             "pit_aligned" : 100,
             "yaw_aligned" : 100
            }

ALIGN_ARM.arm_stop = Namespace(**_arm_stop)

# Gains for the TMS to arm cavity servo
ALIGN_ARM.arm_G_pit = -0.1	#-0.5
ALIGN_ARM.arm_G_yaw = -0.1	#-0.5

### Nominal camera centroid values
ALIGN_ARM.cam_spots_nom = {'ITM':{'P':38.0,'Y':-75.2},
                           'ETM':{'P':-18.7,'Y':-1.7}}

ALIGN_ARM.cam_dof_num = {'I':11,'E':13}

# Sus calibrations, TMS is just top stage (gain in opticalign bank),
# ETM and ITM is L1 Lock to M0 OpticAlign
ALIGN_ARM.sus_cals = {'TMS':{'P':1.0/200,'Y':1.0/200},
                      'ITM':{'P':1.8/400,'Y':1.15/400},
                      'ETM':{'P':1.6/400,'Y':1.2/400}
                      }

#------------------------------------------------------------------------------

# Import everthing to make it a Guardian script:
from ALIGN_ARM import *


# Set a prefix, if necessary.
#prefix = ""


# END.
