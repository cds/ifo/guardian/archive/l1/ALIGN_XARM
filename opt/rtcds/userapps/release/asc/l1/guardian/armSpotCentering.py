import numpy as np
import cdsutils
import time

from isclib.epics_average import EzAvg

DMTRX = [[-1.07,1.0],
         [-1.0,0.78]]

# Drive matrix (dmtrx) should be defined for pitch and yaw

# nomspots should be passed as a dictionary with following form
# {'ITM':{'P':val,'Y':val},'ETM':{'P':val,'Y':val}}

#TODO: add the option of choosing the channel, might use oplev locking.

class CentroidCentering(object):
    """
    A class for setting the arm cavity axis using the camera centroid.
    Only works where ezca (EPICS access) is already defined, such as in
    the Guardian codes.
    """
    def __init__(self,ezca,arm,nomspots,pitDmtrx=DMTRX,yawDmtrx=DMTRX,gain=0.3,tAvg=5.0):
        """
        Usage:
        arm - which arm are we aligning, X or Y
        nomspots - the camera centroid values for the nominal spot positions, should
                   be a dictionary {'ITM':{'P':val,'Y':val},'ETM':{'P':val,'Y':val}}
        """
        self._ezca = ezca
        self._arm = arm
        self._gain = gain
        self._tAvg = tAvg
        self._Dmtrx = {'P':pitDmtrx,'Y':yawDmtrx}
        self._yawDmtrx = yawDmtrx
        self._nomspots = nomspots

        self._ind = {'ITM':0,'ETM':1}

    #------------------------------------------------------------
    def diff_spots(self,dofs):
        """
        returns a dictionary of the different
        """
        cam_dof = {'P':'Y','Y':'X'}

        chans = []
        for tm in ['ITM'+self._arm,'ETM'+self._arm]:
            for dof in dofs:
                chans.append("L1:CAM-{0}_{1}".format(tm,cam_dof[dof]))
        #TODO: use stddev to decide whether to use
        foo = cdsutils.avg(-1*self._tAvg,(chans),stddev=True)

        cam_spots_curr = {}
        i = 0
        for tm in ['ITM','ETM']:
            cam_spots_curr[tm] = {}
            for dof in dofs:
                cam_spots_curr[tm][dof]=foo[i][0]
                i+=1

        # make 2 element array (ITM,ETM) of diffs for each dof (P,Y)
        cam_diff = {}
        for dof in dofs:
            aa = [0,0]
            for tm in ['ITM','ETM']:
                aa[self._ind[tm]] = self._nomspots[tm][dof] - cam_spots_curr[tm][dof]
            cam_diff[dof] = aa
            #cam_diff[dof] = np.array(aa)

        # returns a dictionary of 2 element arrays for every dof
        return cam_diff

    #------------------------------------------------------------
    def calc_step_size(self,dofs):

        cam_diff = self.diff_spots(dofs)

        # Matrix math to work out required step of each TM for each dof,
        # returns a 2 key dictionary (P,Y) of 2 element (ITM,ETM) np arrays
        tm_step = {}
        for dof in dofs:
            tm_step[dof] = 1.0*self._gain*np.array(self._Dmtrx[dof]).dot(np.array(cam_diff[dof]))

        return tm_step

    #------------------------------------------------------------
    def center_arm_spots(self,dofs,tramp=3.0):
        """
        Centers the arm spots by driving the ITM and ETM
        Usage:
        can be one degree of freedom (dof) or multiple.
        If multiple define dof as an array, eg. ['P','Y']
        """
        for tm in ['ITM'+self._arm,'ETM'+self._arm]:
            for dof in dofs:
                ezca['SUS-'+tm+'_M0_OPTICALIGN_'+dof+'_TRAMP']=tramp

        tm_step = self.calc_step_size(dofs)

        time.sleep(0.5)

        for tm in ['ITM','ETM']:
            for dof in dofs:
                ezca['SUS-'+tm+self._arm+'_M0_OPTICALIGN_'+dof+'_OFFSET']+=tm_step[dof][self._ind[tm]]

